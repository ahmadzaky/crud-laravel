@extends('layout.master')

@section('container')
<div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
    <div class="grid grid-cols-1 md:grid-cols-2">
        <div class="p-6">
            <div class="flex items-center">
                <div class="ml-4 text-lg leading-7 font-semibold"><a class="underline text-gray-900 dark:text-white">Edit Barang</a></div>
                <div class="ml-12">
                    @foreach ($barangs as $barang)
                    <form method="POST" action="/edit-barang/{{$barang->id}}">
                        @csrf
                        <input type="text" name="nama_barang" placeholder="Masukan nama barang" value="{{$barang->nama_barang}}"/>
                        <input type="number" name="jumlah" placeholder="Masukan jumlah" value="{{ $barang->jumlah }}"/>
                        <button type="submit">simpan</button>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
