@extends('layout.master')


@section('container')



<div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
    <div class="grid grid-cols-1 md:grid-cols-2">
        <div class="p-6">
            <div class="flex items-center">
                <div class="ml-4 text-lg leading-7 font-semibold"><a class="underline text-gray-900 dark:text-white">Masukan Barang</a></div>
                <div class="ml-12">
                    <form method="POST" action="/store">
                        @csrf
                        <input type="text" name="nama_barang" placeholder="Masukan nama barang" />
                        <input type="number" name="jumlah" placeholder="Masukan jumlah" />
                        <button type="submit">simpan</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-l">
            <div class="flex items-center">
                <div class="ml-4 text-lg leading-7 font-semibold text-gray-900 dark:text-white">Daftar Barang</div>
            </div>
            <div class="ml-12">
                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                    <table style="color: white;">
                        <tbody>
                            <thead>
                                <th>ID Barang</th>
                                <th>Nama Barang</th>
                                <th>Jumlah</th>
                                <th>Opsi</th>
                            </thead>
                            @foreach ($barangs as $barang)
                            <tr>
                                <td>{{$barang->id}}</td>
                                <td>{{ $barang->nama_barang }}</td>
                                <td>{{ $barang->jumlah }}</td>
                                <td> <a href="/edit-barang/{{ $barang->id }}"> edit<a> | <a href="/hapus-barang/{{ $barang->id }}"> hapus <a> </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
