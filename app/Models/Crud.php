<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Crud extends Model
{
    use HasFactory;

    public static function get_barangs()
    {
        // return  $results = DB::select(DB::raw("select * from barang"));
        return $users = DB::table('barang')
            ->select(DB::raw('*'))
            ->get();
    }

    public static function get_datas($id)
    {
        // dapatkan data by id dan tampilkan semua datanya
        return $data = DB::table('barang')
            ->select(DB::raw('*'))->where('id', $id)->get();
    }


    public static function update_datas($id, $nama_barang, $jumlah)
    {
        // update data by id
        return $update = DB::table('barang')
            ->where('id', $id)
            ->update([
                'nama_barang' => $nama_barang,
                'jumlah' => $jumlah
            ]);
    }

    public static function store_barang($nama_barang, $jumlah)
    {
        return $insert =   DB::table('barang')->insert([
            'nama_barang' => $nama_barang,
            'jumlah' => $jumlah
        ]);
    }


    public static function hapus_barang($id)
    {
        return $dele = DB::delete('delete from barang where id = ?', [$id]);
    }
}
