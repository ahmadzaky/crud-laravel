<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Models\Crud;
use Illuminate\Http\Request;
//use Input, Auth, Redirect, View, Validator, Excel;

class CrudController extends BaseController
{
    //function untuk menampilkan halaman
    public function menampilkan_form_input_barang()
    {
        $get_data = Crud::get_barangs();

        return view('crud', ['barangs' => $get_data]);
    }

    //function untuk store barang
    public function simpan_data_barang(Request $request)
    {
        $nama_barang = $request->input('nama_barang');
        $jumlah = $request->input('jumlah');

        $insert = Crud::store_barang($nama_barang, $jumlah);

        $get_data = Crud::get_barangs();
        return view('crud', ['barangs' => $get_data]);
    }

    //function untuk tampilan edit barang
    public function edit_barang(Request $request)
    {
        $get_data = Crud::get_datas($request->id);
        return view('edit', ['barangs' => $get_data]);
    }

    //function untuk update data barang
    public function update_barang(Request $request)
    {
        $nama_barang = $request->input('nama_barang');
        $jumlah = $request->input('jumlah');

        $update  = Crud::update_datas($request->id, $nama_barang, $jumlah);
        return redirect('/');
    }


    // untuk hapus barang
    public function hapus_barang(Request $request)
    {
        $dele = Crud::hapus_barang($request->id);
        return redirect('/');
    }
}
