<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CrudController;
use App\Http\Controllers\TestController;

// use App\Http\Controllers\CrudController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [CrudController::class, 'menampilkan_form_input_barang']);
Route::post('/store', [CrudController::class, 'simpan_data_barang']);
Route::get('/edit-barang/{id}', [CrudController::class, 'edit_barang']); //tampilkan halaman edit
Route::post('/edit-barang/{id}', [CrudController::class, 'update_barang']); // untuk update data barang

Route::get('/hapus-barang/{id}', [CrudController::class, 'hapus_barang']); //terserah mau method post atau get
